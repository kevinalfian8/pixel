package com.kevin.pixeltest

import android.graphics.Bitmap
import android.graphics.Color
import android.os.Bundle
import android.util.DisplayMetrics
import androidx.activity.enableEdgeToEdge
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.ViewCompat
import androidx.core.view.WindowInsetsCompat
import com.kevin.pixeltest.databinding.ActivityBitmapBrightnessCheckBinding

class BitmapBrightnessCheckActivity : AppCompatActivity() {

    private lateinit var binding: ActivityBitmapBrightnessCheckBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = ActivityBitmapBrightnessCheckBinding.inflate(layoutInflater)

        setContentView(binding.root)

        val image = createBitmap()

        binding.ivBefore.setImageBitmap(Bitmap.createScaledBitmap(image, 200, 200, false))
        binding.tvResult.text = image.checkIsDark()

    }

    private fun createBitmap(): Bitmap {
        val colorArray = intArrayOf(
            Color.rgb(87, 76, 63),
            Color.rgb(67, 76, 73),
            Color.rgb(99, 105, 93),
            Color.rgb(178, 173, 169),
            Color.rgb(48, 35, 46),

            Color.rgb(22, 20, 18),
            Color.rgb(10, 40, 50),
            Color.rgb(67, 76, 73),
            Color.rgb(173, 166, 167),
            Color.rgb(87, 76, 63),

            Color.rgb(10, 40, 50),
            Color.rgb(99, 105, 93),
            Color.rgb(178, 173, 169),
            Color.rgb(67, 76, 73),
            Color.rgb(22, 20, 18),

            Color.rgb(22, 20, 18),
            Color.rgb(87, 76, 63),
            Color.rgb(140, 132, 139),
            Color.rgb(87, 76, 63),
            Color.rgb(99, 105, 93),

            Color.rgb(99, 105, 93),
            Color.rgb(87, 76, 63),
            Color.rgb(67, 76, 73),
            Color.rgb(173, 166, 167),
            Color.rgb(48, 35, 46),
        )

        val bitmap = Bitmap.createBitmap(colorArray, 5, 5, Bitmap.Config.ARGB_8888)

        return bitmap
    }
}