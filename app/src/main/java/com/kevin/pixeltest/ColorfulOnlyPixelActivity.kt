package com.kevin.pixeltest

import android.graphics.Bitmap
import android.graphics.Color
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.kevin.pixeltest.databinding.ActivityColorfulOnlyPixelBinding

class ColorfulOnlyPixelActivity : AppCompatActivity() {

    private lateinit var binding: ActivityColorfulOnlyPixelBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = ActivityColorfulOnlyPixelBinding.inflate(layoutInflater)

        setContentView(binding.root)

        val image = createBitmap()

        binding.ivBefore.setImageBitmap(Bitmap.createScaledBitmap(image, 200, 200, false))

        val imageFiltered = Bitmap.createScaledBitmap(image.colorfulPixelOnly(), 200, 200, false)

        binding.ivResult.setImageBitmap(imageFiltered)

    }

    private fun createBitmap(): Bitmap {
        val colorArray = intArrayOf(
            Color.rgb(34, 203, 55),
            Color.rgb(67, 76, 73),
            Color.rgb(99, 105, 93),
            Color.rgb(178, 173, 169),
            Color.rgb(144, 89, 54),

            Color.rgb(22, 20, 18),
            Color.rgb(10, 40, 50),
            Color.rgb(171, 180, 211),
            Color.rgb(150, 150, 90),
            Color.rgb(50, 150, 150),

            Color.rgb(209, 109, 107),
            Color.rgb(111, 117, 212),
            Color.rgb(214, 113, 165),
            Color.rgb(45, 137, 212),
            Color.rgb(182, 240, 245),

            Color.rgb(199, 184, 72),
            Color.rgb(204, 75, 193),
            Color.rgb(140, 132, 139),
            Color.rgb(87, 76, 63),
            Color.rgb(170, 209, 167),

            Color.rgb(1, 90, 20),
            Color.rgb(174, 214, 174),
            Color.rgb(196, 106, 112),
            Color.rgb(173, 166, 167),
            Color.rgb(48, 35, 46),
        )

        val bitmap = Bitmap.createBitmap(colorArray, 5, 5, Bitmap.Config.ARGB_8888)

        return bitmap
    }
}