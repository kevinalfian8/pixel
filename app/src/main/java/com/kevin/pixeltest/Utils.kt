package com.kevin.pixeltest

import android.R.attr
import android.graphics.Bitmap
import android.graphics.Color
import android.graphics.Matrix
import android.util.DisplayMetrics


fun Int.isDark(): Boolean {
    val darkness = 1 - ((0.299*Color.red(this)) + (0.587*Color.green(this)) + (0.114*Color.blue(this)))/255

    if (darkness < 0.50) {
        return false
    }

    return true
}

fun Int.isBright(): Boolean {
    val hsv = FloatArray(3)
    Color.colorToHSV(this, hsv)

    val brightness = hsv[2]

    if (brightness < 0.5)
        return false

    return true
}

fun Bitmap.checkIsDark(): String {
    val width = this.width
    val height = this.height

    var darkPixel = 0

    for (y in 0..<height) {
        for (x in 0..<width) {
            val pixel = this.getPixel(x, y)

            if (pixel.isDark())
                darkPixel++
        }
    }

    val threshold = darkPixel.toDouble()/(width*height)

    if (threshold > 0.5) {
        return "Dark Image"
    }

    return "Bright Image"
}

fun Bitmap.colorfulPixelOnly(): Bitmap {
    val colorArray = mutableListOf<Int>()

    val width = this.width
    val height = this.height

    for (y in 0..<height) {
        for (x in 0..<width) {
            val pixel = this.getPixel(x, y)

            if (pixel.isBright())
                colorArray.add(pixel)
            else
                colorArray.add(Color.WHITE)
        }
    }

    val bitmap = Bitmap.createBitmap(colorArray.toIntArray(), 5, 5, Bitmap.Config.ARGB_8888)

    return bitmap
}